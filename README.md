# README #

This is a django application designed to run with any django installation including Mezzanine.

### What is this repository for? ###

* FitnessLog follows the book, The Hacker's Diet, and creates an easy to use application for tracking and graphing.
* Version 0.1

### How do I get set up? ###

* Summary of set up
Download the repository from bitbucket into your django project directory where you normally store applications

* Configuration
In your projects urls.py file add the following line to the urlpatterns:
<code>
 url("^fitnesslog/", include('FitnessLog.urls')),
</code>

* Dependencies
numpy==1.11.0
Django==1.9.6

The application has been tested with Python 3.5.1.

### Contribution guidelines ###

For contributions please contact David Post at david@ohyonghao.com

### Who do I talk to? ###

Contact David Post at david@ohyonghao.com