from django.conf.urls import patterns, include, url

from django.contrib import admin
from . import views

urlpatterns = [
    # Examples:
    #url(r'^$', 'OpenDiet.views.home', name='home'),
    #url(r'^$', 'FitnessLog.views.fitness_form', name='log'),
    url(r'^$', views.fitness_form, name='log'),
    url(r'^(?P<page>\d+)$', views.fitness_form, name='log'),
    #url(r'^fitnesslog/$', views.fitness_form, name='log'),
    #url(r'^fitnesslog/(?P<page>\d+)$', views.fitness_form, name='log'),
    url(r'^overview', views.overview, name='overview'),
    url(r'^graph/$', views.graph, name='graph'),
    url(r'^graph/(?P<days>\d+)$', views.graph, name='graph'),
    url(r'^graph/(?P<username>\w+)$', views.graph, name='graph'),
    url(r'^graph/(?P<username>\w+)/(?P<days>\d+)$', views.graph, name='graph'),
    
    #url(r'^blog/', include('blog.urls')),
    #url(r'^login', 'OpenDiet.views.login_view'),
    #url(r'^logout', 'OpenDiet.views.logout_view'),
    #url(r'^register', 'OpenDiet.views.register', name='register'),
    #url(r'^userinfo/$','FitnessLog.views.userinfo_form', name='account' ),
    #url(r'^userinfo/(?P<username>\w+)$','FitnessLog.views.userinfo_form', name='account' ),
    #url(r'^register', 'OpenDiet.views.beta', name='register'),
    #url(r'^admin/', include(admin.site.urls)),
#    url(r'', include('social_auth.urls')),
]
