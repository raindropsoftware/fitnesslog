function bestfit(data) {
	var ii=0, x, y, x0, x1, y0, y1, dx,
	m = 0, b = 0, cs, ns,
	n = data.length, Sx = 0, Sy = 0, Sxy = 0, Sx2 = 0, S2x = 0;
            
	// Not enough data or disabled
	if(n < 2) return;
            
        // Do math stuff
	for(ii;ii<data.length;ii++){
		x = data[ii][0];
		y = data[ii][1];
		Sx += x;
		Sy += y;
		Sxy += (x*y);
		Sx2 += (x*x);
	}
	// Calculate slope and intercept
	m = (n*Sx2 - S2x) != 0 ? (n*Sxy - Sx*Sy) / (n*Sx2 - Sx*Sx) : 0;
	b = (Sy - m*Sx) / n;
            
	// Calculate minimal coordinates to draw the trendline
	x0 = parseFloat(data[0][0]);
	y0 = parseFloat(m*x0 + b);
	x1 = parseFloat(data[ii-1][0]);
	y1 = parseFloat(m*x1 + b);

	// We extend add the new serie to the series array
	//ns = $.extend(true, opts.series, { data:[[x0,y0],[x1,y1]], lines: defaultLine, bars: defaultOther, label: false, points:defaultOther } );
	//series.push(ns);
	return [[x0,y0],[x1,y1]];

}

var t=[];
var tp=[];
var w=[];
var wb=[];
var wa=[];

var prev = [0,0,0];




function calcData(input){
	for( var i = 0; i < input.length; ++i) {
        var timestamp = Date.parse(input[i][0]).getTime();
        var weight = input[i][1];
        var trend = input[i][2];
	var user = input[i][3];

        w.push([timestamp,weight]);
        t.push([timestamp,trend]);
	if(user){
		tp.push([timestamp,trend]);
	}

        if(prev[1] == 0){
                prev = [timestamp,weight,trend];
        }

        if(weight > trend){
                if(prev[1] < prev[2]){
                        wa.push([prev[0],prev[2]]);
                }
                wa.push([timestamp,weight]);
                wb.push([timestamp,trend]);
        }
        else if(weight < trend) {
                if(prev[1] > prev[2]){
                        wb.push([prev[0],prev[2]]);
      	          }
      	          wb.push([timestamp,weight]);
     	          wa.push([timestamp,trend]);
	 }
	else if(weight == trend) {
		wb.push([timestamp,weight]);
		wa.push([timestamp,weight]);
	}

   	 prev = [timestamp,weight,trend];
	}
}

function plotData(input){
	calcData(input);
	var bestFit = bestfit(t);
	$.plot("#placeholder",
                [{
                        data: wa,
                        color: "red",
                        lines: {
                                fill: true,
                                lineWidth: 1,
                        },
                        hoverable: false,
                        fillBetween: "trend"
                },{
                        data: wb,
                        color: "green",
                        lines: {
                                fill: true,
                                lineWidth: 1,

                        },
                        hoverable: false,
                        fillBetween: "trend"
                },{
                        id: "w",
                        data: w,
                        hoverable: false,
                        lines: {
                                show: false
                        }
                },{
                        id: "trend",
                        data: t,
                        color: "blue",
                        hoverable: false,
                        lines: {
                                show: true,
                                lineWidth: 3
                        },
                },{
                        id: "trendPoints",
                        data: tp,
                        color: "blue",
                        hoverable: true,
                        lines: {
                                show: false,
                                lineWidth: 3
                        },
                           points: {
                                show: true,
                                radius: 4,
                          },
                },{
			 id:"bestFit",
                        data: bestFit,
                        color: "black",
			hoverable: false,
                        lines: {
                                show: true,
				lineWidth: 1
                        },
                }],
                {
                        xaxis: {
                                mode: "time",
                                timeformat: "%d %b"
                        },
                        grid: {
                                hoverable: true,
                                clickable: true
                        },
                });
	//alert(bestFit[0][1]-bestFit[1][1]);
	var weightChanged = bestFit[0][1]-bestFit[1][1];
	var defecit = ( weightChanged * 3500 ) / input.length;
	$("span.weight-change").text( weightChanged );
	$("span.defecit").text( defecit );
}

function showTooltip(x, y, contents) {
	$("<div id='tooltip'>" + contents + "</div>").css({
		position: "absolute",
		display: "none",
		top: y + 5,
		left: x + 5,
		border: "1px solid #fdd",
		padding: "2px",
		"background-color": "#fee",
		opacity: 0.80
	}).appendTo("body").fadeIn(200);
}

function getIndex(inputArray,index, key){
	for( var i = 0; i < input.length; ++i) {
		if(inputArray[i][index] == key){
				return i;
		}
	}
	return -1;
}
var previousPoint = null;
$("#placeholder").bind("plothover", function (event, pos, item) {
	if (item) {
		if (previousPoint != item.dataIndex) {
			previousPoint = item.dataIndex;

			$("#tooltip").remove();
			var dt = new Date(item.datapoint[0]);
			var td = dt.toString("M/d/yyyy");
			var tt = item.datapoint[1].toFixed(2);
			var wi = getIndex(w,0,item.datapoint[0]);
			var tw = w[wi][1];

			showTooltip(item.pageX, item.pageY,
			td + "<br/>Current Weight: " + tw + "<br/>Avg Weight: " + tt);
		}
	} else {
		$("#tooltip").remove();
		previousPoint = null;
	}
});
