from django.contrib import admin

# Register your models here.
from FitnessLog.models import UserInfo, FitnessLog

class FitnessLogAdmin(admin.ModelAdmin):
    list_display = ['user', 'log_date', 'weight', 'trend', 'body_fat']
    list_filter = ['user']

admin.site.register(FitnessLog, FitnessLogAdmin)

class UserInfoAdmin(admin.ModelAdmin):
    list_display = ['user', 'weight', 'height', 'birthday', 'metric']
    
admin.site.register(UserInfo, UserInfoAdmin)
