from django.forms import ModelForm, DecimalField, DateField
from FitnessLog.models import FitnessLog, UserInfo

class LogEntry(ModelForm):
    class Meta:
        model = FitnessLog
        fields = ['log_date', 'weight', 'body_fat', 'note']

class UserAccount(ModelForm):
    weight = DecimalField(label="Goal Weight")
    class Meta:
        model = UserInfo
        fields = ['metric', 'weight', 'height', 'birthday', 'public']
        