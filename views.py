from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.core.paginator import Paginator
from django.contrib.auth.models import User
from FitnessLog.forms import LogEntry, UserAccount
from FitnessLog.models import FitnessLog, UserInfo
import datetime
import pprint

@login_required
def overview(request):
    """Overview, displays general info about diet"""
    # Grab userinfo
    userinfo = UserInfo.objects.get(user=request.user)
    try:
        last_log = FitnessLog.objects.order_by('-log_date').filter(user=request.user)[0:1].get()
    except FitnessLog.DoesNotExist:
        return render(request, 'FitnessLog/no_log_data.html' )
        
    return render(request, 'FitnessLog/overview.html', {'day_7': last_log.calc_average(7),
                                             'day_30': last_log.calc_average(30),
                                             'day_90': last_log.calc_average(90),
                                             'metric': userinfo.metric})

@login_required
def fitness_form(request, page="1"):
    pp = pprint.PrettyPrinter(indent=4)
    if request.method == 'POST': # If the form has been submitted...
        if 'delete' in request.POST:
            entry = FitnessLog.objects.get(id = request.POST['id'])
            entry.delete()
            return HttpResponseRedirect('/fitnesslog/') # Redirect after POST
        pp.pprint("Form is POST and not delete")
        data = request.POST.copy()
        if( data['body_fat'] == ''):
            pp.pprint("Body fat not set")
            data['body_fat'] = float(0.0)
        else:
            pp.pprint(data['body_fat'])
        if( float(data['body_fat']) > 0.0 ):
            data['body_fat'] = float(data['body_fat'])/100
        form = LogEntry(data) # A form bound to the POST data
        pp.pprint("Copied data into form")
        if form.is_valid(): # All validation rules pass
            pp.pprint("Form Is Valid")
            # Process the data in form.cleaned_data
            # ...
            # Pull yesterdays trend
            # If no data set trend = form.trend
            # Update future points
            
            # Check if object already exists
            try:
                logged = FitnessLog.objects.get(log_date=form.cleaned_data['log_date'],
                                                      user = request.user)
            except FitnessLog.DoesNotExist:
                logged = FitnessLog(log_date=form.cleaned_data['log_date'], user = request.user)
                
            logged.weight = form.cleaned_data['weight']
            logged.body_fat = form.cleaned_data['body_fat']
            logged.note = form.cleaned_data['note']
            logged.user_inputed = True
            pp.pprint(logged)
            logged.save(update_trend=True)
            
            return HttpResponseRedirect('/fitnesslog/') # Redirect after POST
        else:
            pp.pprint("Form Invalid")
            pp.pprint(form)
    else:
        pp.pprint("Form is not POST")
        form = LogEntry() # An unbound form

    log = FitnessLog.objects.order_by('-log_date').filter(user=request.user)
    pages = Paginator( log, 10 )
    
    # ensure that we don't try for too many pages, or a negative page
    page = int(page)
    if page > pages.num_pages:
        page = pages.num_pages
    elif page < 1:
        page = 1

    current_page = pages.page(page)
    if log.count() == 0:
        next_date = datetime.date.today
        last_weight = 0.0
        last_body_fat = 0.0
    else:
        next_date = log[0].log_date + datetime.timedelta(days=1)
        last_weight = log[0].weight
        last_body_fat = float(0.0)
    return render(request, 'FitnessLog/fitnesslog.html', {
        'form': form,
        'log': current_page.object_list,
        'pages': pages,
        'current_page': current_page,
        'next_date': next_date,
        'last_weight': last_weight,
        'last_body_fat': last_body_fat,
    })

def graph(request, days=30, username=''):
    """Graph placeholder"""
    days = int(days)
    
    if username == '':
        user = request.user
    else :
        try :
            user = User.objects.filter(username=username)[0]
        except IndexError:
            return render( request, 'FitnessLog/graph.html', {'error': "User Does Not Exist or Has Not Shared Publicaly"} )

    try :
        last_log = FitnessLog.objects.order_by('-log_date').filter(user=user)[0:1].get()
    except FitnessLog.DoesNotExist:
            return render( request, 'FitnessLog/graph.html', {'error': "User has no log entries" })
    days_ago = last_log.log_date - datetime.timedelta(days=days)

    logs = last_log.entries_between_dates( days_ago, last_log.log_date )
    # Grab userinfo
    userinfo = UserInfo.objects.get(user=user)
   
    return render(request, 'FitnessLog/graph.html', {'log': logs, 'metric': userinfo.metric, 'username': user.username})

@login_required
def userinfo_form(request, username=''):
    if(username==''):
        user = request.user
    else:
        try :
            user = User.objects.get(username=username)
        except User.DoesNotExist:
            return render( request, 'FitnessLog/user_account.html', {'error': "User Does Not Exist or Has Not Shared Publicaly"} )

    try:
        userinfo = UserInfo.objects.get(user=user)
    except UserInfo.DoesNotExist:
        return render( request, 'FitnessLog/user_profile.html', {'error': "User Does Not Exist or Has Not Shared Publicaly"} )
    
    if(user != request.user):
        # Display info only
        return render( request, 'FitnessLog/user_profile.html')
    
    if request.method == 'POST': # If the form has been submitted...
        form = UserAccount(request.POST) # A form bound to the POST data
        if form.is_valid(): # All validation rules pass
            # Process the data in form.cleaned_data
            # ...
            # Check if object already exists  
            userinfo.weight = form.cleaned_data['weight']
            userinfo.metric = form.cleaned_data['metric']
            userinfo.height = form.cleaned_data['height']
            userinfo.birthday = form.cleaned_data['birthday']
            userinfo.public = form.cleaned_data['public']
            userinfo.save()
            
            return HttpResponseRedirect('/userinfo/'+user.username) # Redirect after POST
        else:
            # Fix the date for birthday to display correctly the second time through

            formdata = form.data.copy()
            formdata['birthday'] = datetime.datetime.strptime(form.data['birthday'],"%Y-%m-%d")
            form = UserAccount(formdata)
    else:
        form = UserAccount(instance=userinfo) # An unbound form
        
    return render(request, 'FitnessLog/user_account.html', {
        'form': form,
        })
